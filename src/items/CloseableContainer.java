package items;

import interfaces.Closeable;
import textadventure.World;

public class CloseableContainer extends Container implements Closeable {
	private boolean isOpen;
	public CloseableContainer(World world, String name, String description, boolean _isOpen) {
		super(world, name, description);
		isOpen = _isOpen;
		// TODO Auto-generated constructor stub
	}
	@Override
	public boolean isOpen() {
		// TODO Auto-generated method stub
		return isOpen;
	}
	@Override
	public void doOpen() {
		// TODO Auto-generated method stub
		isOpen = Closeable.OPEN;
		World.print("Opened. \n\n");
	}
	@Override
	public void doClose() {
		// TODO Auto-generated method stub
		isOpen = Closeable.CLOSED;
		World.print("Closed. \n\n");
	}

	@Override
	public void doExamine() {
		// TODO Auto-generated method stub
		if(!isOpen)
		{
			World.print("The " + this.toString() + " is closed. \n\n");
		}
		else
		{
			World.print("Inside the " + this.toString() + " you see: " + getItemString() + "\n\n");
			
		}
	}
	
	public String toString()
	{
		return getName();
	}
	@Override
	public Item doTake(Item item)
	{
		if(isOpen)
		{
			return super.doTake(item);
		}
		else
		{
			World.print("The " + this.toString() + " is closed. \n\n");
			return null;
		}
	}
	@Override
	public Item doPut(Item item, Container source) {
		if(isOpen)
		{
			return super.doPut(item, source);
		}
		else
		{
			World.print("The " + this.toString() + " is closed. \n\n");
			return null;
		}
			
	}
	
}
