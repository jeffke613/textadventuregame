package items;

import interfaces.Edible;
import textadventure.Player;
import textadventure.World;

public class Food extends Item implements Edible{
	
	public Food(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doEat()
	{
		World.print("You eat the " + this.getName() + " and feel stronger!\n\n");
		getWorld().getPlayer().removeItem(this);
		Player player = getWorld().getPlayer();
		getWorld().getPlayer().setHealth(player.getHealth() + this.getWeight());
		player.setBrushedTeeth(false);
	}
	
	@Override
	public void doUse()
	{
		doEat();
	}
}
