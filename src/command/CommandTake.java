
package command;

import items.Container;

import items.Item;
import textadventure.World;

public class CommandTake extends Command {

	@Override
	public String[] getCommandWords() {
		return new String[]{"take", "get", "grab", "hold"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		if (params.length == 1) 
		{
			String itemName = params[0];
			if (world.getPlayer().getCurrentRoom().hasItem(itemName))
			{
				Item item = world.getPlayer().getCurrentRoom().getItem(itemName);
				item.doTake(world.getPlayer().getCurrentRoom());
			} 
			else if (world.getPlayer().hasItem(itemName)) 
			{
				World.print("You already have that!\n\n");
			} 
			else {
				World.print("You can't see any " + itemName + " here.\n\n");
			}
		}
		// Handle "take [Item] from [Container]" command
		else if(params.length == 3) 
		{
			// Handle errors
			if (!params[1].equals("from"))
			{
				world.print("I don't understand." + "\n\n");
				return;
			}
			if (!world.getPlayer().hasItem(params[2]) && !world.getPlayer().getCurrentRoom().hasItem(params[2]))
			{
				world.print("You can't see any " + params[2] + " here." + "\n\n");
				return;
			}
			if (!(world.getPlayer().getItem(params[2]) instanceof Container) && !(world.getPlayer().getCurrentRoom().getItem(params[2]) instanceof Container))
			{
				world.print("The " + params[2] + " can\'t hold things!" + "\n\n");
				return;
			}
			Container container;
//			System.out.println(world.getPlayer().hasItem(params[2]));
//			System.out.println(world.getPlayer().getCurrentRoom().hasItem(params[2]));
			container = (Container) world.getPlayer().getCurrentRoom().getItem(params[2]);
			if(container == null)
			{
				System.out.println("bruh");
			}
			if (!container.hasItem(params[0]))
			{
				world.print("The " + params[2] + " doesn't have a " + params[0]);
				return;
			}
		
		// If we made it this far, then it's safe to
		// take [item] from [container]
			
			container.doTake(container.getItem(params[0]));
		}

		else {
			World.print("I don't understand.\n\n");
		}
	}

	@Override
	public String getHelpDescription() {
		return "[item] or [item] from [container]";
	}

}
