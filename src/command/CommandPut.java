package command;

import items.*;
import textadventure.Player;
import textadventure.Room;
import textadventure.World;

public class CommandPut extends Command {

	@Override
	public String[] getCommandWords() {
		// TODO Auto-generated method stub
		return new String[] {"put", "place"};
	}
	
	@Override
	public void doCommand(String cmd, String[] params, World world) {
		// TODO Auto-generated method stub
		//put [item] in [container]
		if(!params[1].equals("in") || params.length < 3)
		{
			world.print("Invalid syntax\n\n");
			return;
		}
		Player player = world.getPlayer();
		Room currentRoom = player.getCurrentRoom();
		String containerName = params[2];
		String itemName = params[0];
		boolean playerHasContainer = world.getPlayer().hasItem(containerName);
		boolean worldHasContainer = world.getPlayer().getCurrentRoom().hasItem(containerName);
		
		boolean playerHasItem = world.getPlayer().hasItem(itemName);
		boolean worldHasItem = world.getPlayer().getCurrentRoom().hasItem(itemName);
		if(!playerHasItem && !worldHasItem)
		{
			world.print("You can't see any " + params[0] + "here");
			return;
		}
		if(!playerHasContainer && !worldHasContainer)
		{
			world.print("You can't see any " + params[2] + "here");
			return;
		}
		Item container = (playerHasContainer) ? world.getPlayer().getItem(containerName) : world.getPlayer().getCurrentRoom().getItem(containerName);
		if(!(container instanceof Container))
		{
			world.print("The " + params[2] + " can't hold things. ");
			return;
		}
		if(params[0].equals(params[2]))
		{
			world.print("You can\'t put the " + params[2] + " in itself!");
			return;
		}
//		container = (Container)container;
		Item item = (playerHasItem) ? world.getPlayer().getItem(itemName) : world.getPlayer().getCurrentRoom().getItem(itemName);
		//we chill now
		if(player.hasItem(params[0]))
		{
			((Container) container).doPut(item, player);
		}
		else
		{
			((Container) container).doPut(item, currentRoom);
		}
	}

	@Override
	public String getHelpDescription() {
		
		return "put [item] into [container]";
	}
}
