/**
 * Second Lab reflection.
 * Jeffrey Ke, Period 4, 2/28/2020
 * This lab took me (another) 2 hours.
 * The HW #2 finally works! I went to Mr. Ferrante
 * to see how I could fix my fridge not-opening problem,
 * and it turns out it was because I didn't declare
 * the fridge as a fridge object, which was why
 * I kept getting the "You can't open that!" message.
 * The rest of the lab went smoothly, and a feature of Eclipse
 * that I really appreciated was how I could just automatically
 * implement Overrided methods and just fill in the code. That
 * made things much faster. 
 */


/**
 * Lab reflection.
 * Jeffrey Ke, Period 4, 2/27/2020
 * This lab took me 3.5 hrs
 * This lab doesn't work yet! The part I'm stuck on is
 * making the fridge closeable and openable! All it tells
 * me is "You can't open that", even though it implements
 * the Closeable interface.
 */

import textadventure.DemoWorld;

public class RunGame {
	
	public static void main(String[] args) {
		new DemoWorld().play();
	}	
}